# githome

Helper script for creating a repository on the server.

## Prerequisites

- A [git server](https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server) on the machine

## Usages

- `new-repo` a script for creating a new git repository
